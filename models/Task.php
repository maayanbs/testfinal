<?php

namespace app\models;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\behaviors\BlameableBehavior;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $name
 * @property int $urgency
 * @property string $created_at
 * @property string $updated_at
 * @property int $crated_by
 * @property int $updated_by
 */
class Task extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['urgency', 'crated_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'urgency' => 'Urgency',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'crated_by' => 'Crated By',
            'updated_by' => 'Updated By',
        ];
    }
            public function behaviors()
    {
        return [

          'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'crated_by',
                'updatedByAttribute' => 'updated_by',
            ], 
       
        ];
    }
        public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

//   public function getUser()
   // {
  //     return $this->hasOne(Users::className(), ['id' => 'user_id']);
 //  }
    public function getUrgency()
    {
        return $this->hasOne(Urgency::className(), ['id' => 'urgency_id']);
    }
}
