<?php

use yii\db\Migration;

/**
 * Class m180624_071738_init_rbac
 */
class m180624_071738_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    $auth = Yii::$app->authManager;

 ///////////הגדרת התפקידים /////////////

    $manager= $auth->createRole('manager');
      $auth->add($manager);
              
      $employee = $auth->createRole('employee');
      $auth->add($employee);

////////////////הגדרת ירושות///////////
      $auth->addChild($manager, $employee);

 //////////////// הגדרת ההרשאות //////////////////
      $updateUsers= $auth->createPermission('updateUsers');
      $auth->add($updateUsers);

     $deleteTask = $auth->createPermission('deleteTask');
      $auth->add($deleteTask); 

      $updateTask = $auth->createPermission('updateTask');
      $auth->add($updateTask);   
              
      $createTask = $auth->createPermission('createTask');
      $auth->add($createTask);

      $updateOwnditails = $auth->createPermission('updateOwnditails'); ////צפיה בפרטים של עצמך בלבד- תנאי מורכב יותר 
      $rule = new \app\rbac\EmployeeRule;
      $auth->add($rule);
      $updateOwnditails->ruleName = $rule->name;                
      $auth->add($updateOwnditails);                 
        

//////////////////////קביעה מי יכול ליישם איזה חוק
      $auth->addChild($manager, $updateUsers);
      $auth->addChild($manager, $deleteTask);
      $auth->addChild($manager, $updateTask); 
      $auth->addChild($employee, $createTask); 
      $auth->addChild($updateOwnditails, $updateUsers);   
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_071738_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_071738_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
