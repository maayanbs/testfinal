<?php

use yii\db\Migration;

/**
 * Class m180624_060101_insert_urgency_table
 */
class m180624_060101_insert_urgency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
$this->insert('urgency',array(
         'id' =>'1',
         'name' => 'low',
  ));
$this->insert('urgency',array(
         'id' =>'2',
         'name' => 'normal',
  ));
  $this->insert('urgency',array(
         'id' =>'3',
         'name' => 'critical',
  ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_060101_insert_urgency_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_060101_insert_urgency_table cannot be reverted.\n";

        return false;
    }
    */
}
